from nltk.sentiment.vader import SentimentIntensityAnalyzer
import xml.etree.ElementTree as et
from functools import reduce
import re


def clean_html_text(raw_html):
    pattern = re.compile('<.*?>')
    text = re.sub(pattern, '', raw_html)
    return re.sub('[\s\-–=&:,!|#„”"\'`@\?;/\$%\\\(\)\{\}\[\]\.\+\*0-9]+', ' ', text).lower()


doc_root = et.parse('Posts.xml').getroot()

posts = []
for row in doc_root.findall('row'):
    post = row.get('Body')
    posts.append(clean_html_text(post))


sid = SentimentIntensityAnalyzer()
for post in posts:
    print(post)
    ss = sid.polarity_scores(post)
    for k in sorted(ss):
        print('{0}: {1}, '.format(k, ss[k]), end='')
    print()
